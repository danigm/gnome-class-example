This is a gnome-class GIR generation example. There's a source file that uses
gnome-class to compile to a shared library and then a small python script to
test that the .gir file generated is working correctly.

## How to test

1. Build with cargo and generate the typelib:

```sh
./build-gir.sh
```

1. Run the tests

```sh
./test.sh
```