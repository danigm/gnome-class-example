#!/usr/bin/env python3

import unittest
from gi.repository import Counter

class TestCounter(unittest.TestCase):

    def test_new(self):
        c = Counter.Counter()
        self.assertEqual(c.get(), 0)

    def test_add(self):
        c = Counter.Counter()
        self.assertEqual(c.add(1), 1)
        self.assertEqual(c.add(3), 4)

    def test_get(self):
        c = Counter.Counter()
        self.assertEqual(c.get(), 0)
        self.assertEqual(c.add(1), 1)
        self.assertEqual(c.get(), 1)

if __name__ == '__main__':
    unittest.main()
